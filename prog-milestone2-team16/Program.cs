﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_team16
{

    class Program
    {

/*———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/
        
        /* GUI objects that need to be accessed from anywhere. */
        static string gui_line_bottomhalfblock = "▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄";
        static string gui_line_sideblocks      = "▌                                                                                                                    ▐";
        static string gui_line_tophalfblock    = "▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀";
        static string gui_line_hyphen          = "----------------------------------------------------------------------------------------------------------------------";

        /* Combinations of above GUI objects to make creating menus easier. */
        static string gui_menu_top    = "▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄\n ▌                                                                                                                    ▐";
        static string gui_menu_bottom = "▌                                                                                                                    ▐\n ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀";
        
        static bool exit_program = false; /* Used by both MenuOperator() and Quit(). */
        static System.DateTime today = DateTime.Today;

/*———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        static void Main(string[] args)
        {
            MenuOperator("m");
        }

/*———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        static void MenuOperator(string input)
        {
            do
            {
                switch (input)
                {
                    case "1":
                        MenuDisplay("1");
                        break;

                    case "2":
                        MenuDisplay("2");
                        break;

                    case "3":
                        MenuDisplay("3");
                        break;

                    case "4":
                        MenuDisplay("4");
                        break;

                    case "m":
                    case "M":
                        MenuDisplay("Main");
                        break;

                    case "q":
                    case "Q":
                        MenuDisplay("Quit");
                        break;

                    default:
                        Msg_InvalidInput("a number 1 to 4 or Q", input);

                        input = "m";
                        break;
                }
            } while (exit_program != true);
        }

        static void MenuDisplay(string show)
        {
            var input = "";
            /* These two strings aren't public like gui_menu_top and gui_menu_bottom because they are used only in this method. */
            string gui_menu_taskpart1 = $"{gui_line_sideblocks}\n ▌ Please choose an option from below and press [Enter]:                                                              ▐\n {gui_line_sideblocks}\n ▌ Option \tDescription                                                                                           ▐\n ▌ ------ \t-----------                                                                                           ▐";
            string gui_menu_taskpart2 = $"▌ N \t\tReturn to Main Menu                                                                                   ▐\n {gui_line_sideblocks}\n ▌ Q \t\tQuit Program                                                                                          ▐\n {gui_menu_bottom}";

            /* Show main menu. */
            if (show == "main" || show == "Main")
            {
                Console.Clear();

                Console.WriteLine($" {gui_menu_top}");
                Console.WriteLine(" ▌                                                     MAIN  MENU                                                     ▐");
                Console.WriteLine($" {gui_line_sideblocks}");
                Console.WriteLine(" ▌ Welcome to Team 16's Milestone 2!                                                                                  ▐");
                Console.WriteLine($" {gui_line_sideblocks}");
                Console.WriteLine(" ▌ Please choose an option from below and press [Enter]:                                                              ▐");
                Console.WriteLine($" {gui_line_sideblocks}");
                Console.WriteLine(" ▌ Option \tDescription \t\t\tAuthor                                                                ▐");
                Console.WriteLine(" ▌ ------ \t------------------------- \t--------------------                                                  ▐");
                Console.WriteLine(" ▌ 1 \t\tDate Calculator \t\tMatt. Campbell                                                        ▐");
                Console.WriteLine(" ▌ 2 \t\tCalculates Grade Average \tBhim Magar                                                            ▐");
                Console.WriteLine(" ▌ 3 \t\tGenerate a Random Number \tEugene Warren-Murphy                                                  ▐");
                Console.WriteLine(" ▌ 4 \t\tRate Favourite Foods \t\tRegan Barton                                                          ▐");
                Console.WriteLine($" {gui_line_sideblocks}");
                Console.WriteLine(" ▌ Q \t\tQuit Program                                                                                          ▐");
                Console.WriteLine($" {gui_menu_bottom}");

                Console.WriteLine("\n   Your choice:");
                input = Console.ReadLine();

                /* Sends user input from main menu to MenuOperator(), which will decide what to do with it. */
                MenuOperator(input);
            }

            /* Show task menu. */
            else if (show == "1" || show == "2" || show == "3" || show == "4")
            {
                Console.Clear();

                Console.WriteLine($" {gui_menu_top}");
                Console.WriteLine($" ▌                                                       Task {show}                                                       ▐");
                if (show == "1")
                {
                    Console.WriteLine(" ▌                                                  Date  Calculator                                                  ▐");
                } else if (show == "2")
                {
                    Console.WriteLine(" ▌                                              Calculates Grade Average                                              ▐");
                } else if (show == "3")
                {
                    Console.WriteLine(" ▌                                              Generate a Random Number                                              ▐");
                } else if (show == "4")
                {
                    Console.WriteLine(" ▌                                                Rate Favourite Foods                                                ▐");
                }
                Console.WriteLine($" {gui_menu_taskpart1}");
                Console.WriteLine($" ▌ Y \t\tRun Task {show}                                                                                            ▐");
                Console.WriteLine($" {gui_menu_taskpart2}");

                Console.WriteLine("\n   Your choice:");
                input = Console.ReadLine();

                switch (input)
                {
                    case "y":
                    case "Y":
                        if (show == "1")
                        {
                            Run_Task1();
                        } else if (show == "2")
                        {
                            /* Run_Task2(); */
                        } else if (show == "3")
                        {
                            /* Run_Task3(); */
                        } else if (show == "4")
                        {
                            Run_Task4();
                        }
                        break;

                    case "n":
                    case "N":
                        Msg_BacktoMain();
                        break;

                    case "q":
                    case "Q":
                        MenuOperator("q");
                        break;

                    default:
                        Msg_InvalidInput("a Y, N or Q", input);

                        MenuOperator(show);
                        break;
                }
            }

            /* Show quit menu. */
            else if (show == "quit" || show == "Quit")
            {
                Console.Clear();

                Console.WriteLine($" {gui_menu_top}");
                Console.WriteLine(" ▌                                                        QUIT                                                        ▐");
                Console.WriteLine($" {gui_line_sideblocks}");
                Console.WriteLine(" ▌ Do you really want to exit the program?                                                                            ▐");
                Console.WriteLine($" {gui_line_sideblocks}");
                Console.WriteLine(" ▌ Option \tDescription                                                                                           ▐");
                Console.WriteLine(" ▌ ------ \t-----------                                                                                           ▐");
                Console.WriteLine(" ▌ Y \t\tQuit Now                                                                                              ▐");
                Console.WriteLine(" ▌ N \t\tReturn to Main Menu                                                                                   ▐");
                Console.WriteLine($" {gui_menu_bottom}");

                Console.WriteLine("\n   Your choice:");
                input = Console.ReadLine();

                switch (input)
                {
                    case "y":
                    case "Y":
                        exit_program = true;
                        break;

                    case "n":
                    case "N":
                        Msg_BacktoMain();
                        break;

                    default:
                        Msg_InvalidInput("a Y or an N", input);

                        MenuOperator("q");
                        break;
                }
            }
        }

/*———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        static void Msg_BacktoMain()
        {
            Console.Clear();

            Console.WriteLine($" {gui_line_bottomhalfblock}");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"\n   Returning to main menu.");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine($"\n {gui_line_tophalfblock}");

            Console.WriteLine("\n   Press any key to continue . . .");
            Console.ReadKey();

            MenuOperator("m");
        }

        static void Msg_InvalidInput(string valid_options, string input)
        {
            Console.Clear();

            Console.WriteLine($" {gui_line_bottomhalfblock}");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"\n   User Error: I expected {valid_options} but you gave me {input}.");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine($"\n {gui_line_tophalfblock}");

            Console.WriteLine("\n   Press any key to try again . . .");
            Console.ReadKey();
        }

/*———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        static void Run_Task1()
        {
            Console.Clear();

            /*#######################################################################################################################*/
            /*# Task 1 - Part 1:    Display today's date.                                                                           #*/
            /*#######################################################################################################################*/
            Console.WriteLine("   Today's date is " + today.ToString("dd/MM/yyyy" + "."));
            Console.WriteLine($"\n {gui_line_hyphen}");

            /*#######################################################################################################################*/
            /*# Task 1 - Part 2:    Calculate how many days old the user is, based on their date-of-birth.                          #*/
            /*#######################################################################################################################*/
            bool dob_validformat = false;
            var dob = new DateTime();

            do
            {
                Console.WriteLine("\n   Enter your date of birth (DoB) in dd/mm/yyyy format:");
                var input = Console.ReadLine();
                var temp1 = DateTime.TryParse(input, out dob);

                /* Is user's answer a date? */
                if (temp1 == false)
                {
                    Msg_InvalidInput("a date in dd/MM/yyyy format", input);

                    Console.Clear();

                    Console.WriteLine("   Today's date is " + today.ToString("dd/MM/yyyy" + "."));
                    Console.WriteLine($"\n {gui_line_hyphen}");
                }
                /* Is that date invalid (we are asking for a date of birth, so date should be in the past not in the future)? */
                else if (dob > today.AddDays(-1))
                {
                    Msg_InvalidInput("a date before today (" + today.ToString("dd/MM/yyyy") + ")", input);

                    Console.Clear();

                    Console.WriteLine("   Today's date is " + today.ToString("dd/MM/yyyy" + "."));
                    Console.WriteLine($"\n {gui_line_hyphen}");
                }
                else
                {
                    dob_validformat = true;
                }

            } while (dob_validformat != true);

            Console.WriteLine($"\n   You were born on " + dob.ToString("dd/MM/yyyy") + " and you are " + (today.Date - dob.Date).Days + " days old.");
            Console.WriteLine($"\n {gui_line_hyphen}");

            /*#######################################################################################################################*/
            /*# Task 1 - Part 3:    Calculates how many days are in X years.                                                        #*/
            /*#######################################################################################################################*/
            bool validformat = false;
            System.DateTime start_date = new DateTime(1, 1, 1);
            long years = 0;
            var days = 0;

            do
            {
                Console.WriteLine("\n   Enter a number higher than zero (0):");
                var input = Console.ReadLine();
                var temp = long.TryParse(input, out years);

                /* Is user's input a number? */
                if (temp == false)
                {
                    Msg_InvalidInput("a number", input);

                    Console.Clear();

                    Console.WriteLine("   Today's date is " + today.ToString("dd/MM/yyyy" + "."));
                    Console.WriteLine($"\n {gui_line_hyphen}");
                    Console.WriteLine($"\n   You were born on " + dob.ToString("dd/MM/yyyy") + " and you are " + (today.Date - dob.Date).Days + " days old.");
                    Console.WriteLine($"\n {gui_line_hyphen}");
                }

                /* Is that input <= 0? */
                else if (years < 1)
                {
                    Msg_InvalidInput("a number > 0", input);

                    Console.Clear();

                    Console.WriteLine("   Today's date is " + today.ToString("dd/MM/yyyy" + "."));
                    Console.WriteLine($"\n {gui_line_hyphen}");
                    Console.WriteLine($"\n   You were born on " + dob.ToString("dd/MM/yyyy") + " and you are " + (today.Date - dob.Date).Days + " days old.");
                    Console.WriteLine($"\n {gui_line_hyphen}");
                }

                /* Or is that input > 9999?
                   If years > 9999, program will crash with an 'ArgumentOutOfRangeException' error. */
                else if (years > 9999)
                {
                    years = 9999;

                    Console.Clear();

                    Console.WriteLine($" {gui_line_bottomhalfblock}");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"\n   Note: {input} has been set to DateTime()'s max year value of 9999.");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine($"\n {gui_line_tophalfblock}");

                    Console.WriteLine("\n   Press any key to continue . . .");
                    Console.ReadKey();

                    Console.Clear();

                    Console.WriteLine("   Today's date is " + today.ToString("dd/MM/yyyy" + "."));
                    Console.WriteLine($"\n {gui_line_hyphen}");
                    Console.WriteLine($"\n   You were born on " + dob.ToString("dd/MM/yyyy") + " and you are " + (today.Date - dob.Date).Days + " days old.");
                    Console.WriteLine($"\n {gui_line_hyphen}");

                    validformat = true;
                }
                else
                {
                    validformat = true;
                }
            } while (validformat != true);

            /* Checks if year is leap year, and adds days to 'days'. */
            for (var i = 0; i < years; i++)
            {
                /* Adds years to date. */
                var a = start_date.AddYears(i);

                /* This line is used for debugging
                Console.WriteLine($"a = {a}"); */

                /* Removes days and months, because we only want the year part of the date. */
                var b = a.Year;

                /* Checks if year is a leap year. */
                if (DateTime.IsLeapYear(b))
                {
                    days += 366;
                }
                else
                {
                    days += 365;
                }
            }

            Console.WriteLine($"\n   There are {days} days in {years} years.");

            Console.WriteLine($"\n {gui_line_hyphen}");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"\n   Finished Task 1!");
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine("\n   Press any key to return to Main Menu . . .");
            Console.ReadKey();

            MenuOperator("m");
        }

/*———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

        static void Run_Task4()
        {
            var l = 1;
            do
            {
                Console.Clear();
                var food = new SortedDictionary<string, string>();
                var rating = new Dictionary<string, int>();

                Console.WriteLine("   Write your 5 favourite foods.");
                food.Add("one", Console.ReadLine());
                food.Add("two", Console.ReadLine());
                food.Add("three", Console.ReadLine());
                food.Add("four", Console.ReadLine());
                food.Add("five", Console.ReadLine());

                Console.Clear();

                Console.WriteLine($"   These are the Foods you chose. {food["one"]}, {food["two"]}, {food["three"]}, {food["four"]}, {food["five"]} ");
                Console.Clear();

                Console.WriteLine($"   Rate {food["one"]} out of 10.");
                rating.Add("one", int.Parse(Console.ReadLine()));

                Console.WriteLine($"   Rate {food["two"]} out of 10.");
                rating.Add("two", int.Parse(Console.ReadLine()));

                Console.WriteLine($"   Rate {food["three"]} out of 10.");
                rating.Add("three", int.Parse(Console.ReadLine()));

                Console.WriteLine($"   Rate {food["four"]} out of 10.");
                rating.Add("four", int.Parse(Console.ReadLine()));

                Console.WriteLine($"   Rate {food["five"]} out of 10.");
                rating.Add("five", int.Parse(Console.ReadLine()));

                Console.Clear();

                Console.WriteLine($"   You gave {food["one"]} a rating of {rating["one"]} out of 10");
                Console.WriteLine($"   You gave {food["two"]} a rating of {rating["two"]} out of 10");
                Console.WriteLine($"   You gave {food["three"]} a rating of {rating["three"]} out of 10");
                Console.WriteLine($"   You gave {food["four"]} a rating of {rating["four"]} out of 10");
                Console.WriteLine($"   You gave {food["five"]} a rating of {rating["five"]} out of 10");
                Console.WriteLine();
                Console.WriteLine("   Press Enter to continue.");
                Console.ReadLine();
                Console.Clear();

                //I am having trouble figuring out how to sort dictionary so i have not completed this section//

                /*
                Console.WriteLine("Do you want to sort by most favourite food?");
                var prompt = Console.ReadLine();

                var list = rating.OrderBy(Value => Value.Key);



                if(prompt == "Y")
                {
                 
                    Console.WriteLine($"{rating.OrderBy(Value => Value.Key)}");
                }

                if (prompt == "y")
                {

                    Console.WriteLine($"{rating.OrderBy(Key => Key.Value)}");
                }



                Console.ReadLine();
                Console.Clear();
                */
                Console.WriteLine("   Do you want to Rate the food again? Y or N");

                var answer = Console.ReadLine();
                if (answer == "Y")
                {
                    l = 1;
                }
                if (answer == "y")
                {
                    l = 1;
                }

                if (answer == "N")
                {
                    l = 0;
                }
                if (answer == "n")
                {
                    l = 0;
                }
            } while (l > 0);

            MenuOperator("m");
        }
    

/*———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————*/

    }

}